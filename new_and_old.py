from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import time
from bs4 import BeautifulSoup
import re
import math
import os

s = Service('D:/word2vec/drivers/chromedriver.exe')
option = webdriver.ChromeOptions()
option.add_argument("--user-data-dir=" + r"D:/word2vec/User Data/")
option.add_argument('--no-sandbox')
driver = webdriver.Chrome(service=s, options=option)

loanword="dim sum"
sort="newest"
# sort="oldest"
driver.get('https://www.nytimes.com/search?dropmab=false&query="'+loanword+'"&sort='+sort)
time.sleep(1)
loanWords_page = BeautifulSoup(driver.page_source, 'lxml')
pageNum=loanWords_page.find("p",{"data-testid":"SearchForm-status"})
str = pageNum.text
match = re.search('Showing ([0-9,]+) results for', str)
totalLoanWords = match.group(1) if match else None
totalLoanWords = re.sub(",", "", totalLoanWords)
print(totalLoanWords)
clickNum=math.floor(int(totalLoanWords)/10)

def WriteMe(totalLoanWords,loanword,sort):
    driver.get('https://www.nytimes.com/search?dropmab=false&query="' + loanword + '"&sort=' + sort)
    with open('urls+'+totalLoanWords+'+'+loanword+'-'+sort+'headless.txt', 'w') as f:
        URLs=list()
        for x in range(clickNum):
            driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            driver.find_element(By.CSS_SELECTOR, 'div.css-vsuiox>button').click()
            time.sleep(5)
            bsobj = BeautifulSoup(driver.page_source, 'lxml')
            for link in bsobj.find_all('a'):
                match = re.search("searchResultPosition=", link.get('href'))
                if match:
                    if (link.get('href') not in URLs):
                        URLs.append(link.get('href'))
                        f.write(link.get('href') + '\n')
            # time.sleep(1)
            f.flush
            os.fsync(f)

    f.close()
    time.sleep(5)   #这个停顿有必要，否则条数少的来不及写入

#
# if (int(totalLoanWords) >= 1000):
#     WriteMe(totalLoanWords, loanword, "newest")
#     time.sleep(2)
#     WriteMe(totalLoanWords, loanword, "oldest")
# else:
WriteMe(totalLoanWords, loanword, "newest")

driver.close()


