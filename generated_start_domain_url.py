import pandas as pd
from datetime import datetime


def datelist(beginDate, endDate):
    # beginDate, endDate是形如‘20160601’的字符串或datetime格式
    date_l = [datetime.strftime(x, '%Y-%m-%d') for x in list(pd.date_range(start=beginDate, end=endDate))]
    # print( date_l)
    return date_l


if __name__ == '__main__':
    dateList = datelist('20070101', '20220816')
    filter_list = []
    for i in range(0, len(dateList)):
        #每隔一天 分一段
        if (i % 1 == 0):
            filter_list.append(dateList[i].replace('-', ''))

    #倒序
    filter_list.reverse()

    target_url_list = []
    # 跳过最后一个  －->　len() -1
    for i in range(0, len(filter_list) - 1):
        origin_url_part_1 = 'https://www.nytimes.com/search?dropmab=true&endDate='
        origin_url_part_2 = '&query=&sort=best&startDate='
        target_url = origin_url_part_1 + filter_list[i] + origin_url_part_2 + filter_list[i + 1]
        target_url_list.append(target_url)
    print(target_url_list)

    with open('resource/start_domain_url_list.txt', 'w') as f:
        for url in target_url_list:
            f.write(str(url))
            f.write('\n')
