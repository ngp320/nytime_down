# coding=utf-8
import datetime
import os
import re
import shutil
import sys
import time
import traceback
import urllib.request
import random

import pyautogui
import requests
from loguru import logger
from lxml import etree

from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from utils.utils import wait_to_visible, xpathGetText, randomSleep, validateTitle, Xpath_wait_click_or_input
import justext

def setLoguru():
    fmt = "<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green> " \
          "<level><red>|</red>" \
          "{level}<red>|</red>" \
          "{file}<red>:</red> " \
          "{function} <red>:</red>" \
          "L{line} " \
          "{message}</level> "

    #  多线程 就 显示线程吧, 现在已经有点拥挤了
    # fmt = "<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green> " \
    #       "<level><red>|</red> " \
    #       "{level} <red>|</red> " \
    #       "{thread.name}<red>:</red>" \
    #       "{thread.id} <red>|</red> " \
    #       "{file}<red>:</red> " \
    #       "{function} <red>:</red>" \
    #       "L{line} - " \
    #       "{message}</level> "
    # logger.add 不能分多行
    logger.remove(handler_id=None)  # 清除之前的设置
    # logger.add(sys.stderr, format=fmt, level="INFO", colorize=True)
    logger.add(sys.stderr, format=fmt, level="DEBUG", colorize=True)
    # 设置生成日志文件，每天10MB切割，保留3天，utf-8编码，异步写入，zip压缩
    logger.add("logs/runtime.log", level="DEBUG", backtrace=True, rotation='10MB', retention='3 days',
               encoding="utf-8", enqueue=True, compression="zip")
    # log日志 分几层, 后期容易处理
    logger.add("logs/runError.log", level="ERROR", encoding="utf-8")
    logger.add("logs/runWarning.log", level="WARNING", encoding="utf-8")
    logger.add("logs/runInfo.log", level="INFO", encoding="utf-8")


def chrome_init():
    logger.info("正在启动 chrome 浏览器...(每次启动程序前, 需确定 chrome 已关闭)")
    setLoguru()
    # 启用带插件的浏览器
    from selenium import webdriver
    option = webdriver.ChromeOptions()
    option.add_argument("--user-data-dir=" + r"C:/Users/ngp/AppData/Local/Google/Chrome/User Data/")
    # driver = webdriver.Chrome(executable_path=".\drivers\chromedriver.exe")     # 打开chrome浏览器
    path = os.path.abspath("drivers\chromedriver.exe")
    driver = webdriver.Chrome(executable_path=path, chrome_options=option)  # 打开有插件的chrome浏览器
    # driver.maximize_window()
    # 注入jquery
    with open('resource/jquery-3.6.0.min.js', 'r') as file:
        jquery = file.read()
    driver.execute_script(jquery)
    return driver


def firefox_init():
    logger.info("正在启动 firefox 浏览器...(每次启动程序前, 需确定 firefox 已关闭)")
    setLoguru()
    from selenium import webdriver
    # firefox -> 地址栏 -> 输入 about:profiles -> 本地目录
    profile_directory = r'C:\Users\ngp\AppData\Roaming\Mozilla\Firefox\Profiles\eiyacu9g.default-release'
    profile = webdriver.FirefoxProfile(profile_directory)
    path = os.path.abspath("drivers\geckodriver.exe")
    driver = webdriver.Firefox(firefox_profile=profile, executable_path=path)
    # 注入jquery
    with open('resource/jquery-3.6.0.min.js', 'r') as file:
        jquery = file.read()
    driver.execute_script(jquery)
    return driver


def getHtml(url, referer):  # 获取html的内容
    from utils.userAgents import MY_USER_AGENT
    useragent = random.choice(MY_USER_AGENT)
    proxies = {"http": "http://127.0.0.1:7890", "https": "http://127.0.0.1:7890"}
    headers = {
        'User-Agent': useragent,  ##从self.user_agent_list中随机取出一个字符串
        'Referer': referer,
        'Connection': 'close'  # 关闭长连接
    }  ##构造成一个完整的User-Agent （UA代表的是上面随机取出来的字符串哦）
    html = requests.get(url, headers=headers, proxies=proxies, verify=False)
    return html.text  # http.client.HTTPResponse的变量


def scroll2bottom(driver):
    # 将滚动条移动到页面的底部
    time.sleep(1)
    driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")

def drop_duplicates_txt(filePath):
    tmp = []
    with open(filePath, encoding="utf-8") as file:
        for line in file.readlines():
            line = line.strip('\n')
            tmp.append(line)
    tmp = list(set(tmp))
    tmp = sorted(tmp)
    with open(filePath, 'w', encoding='utf-8') as fileObject:
        for url in tmp:
            fileObject.write(str(url))
            fileObject.write('\n')
        fileObject.close()

def get_list_from_txt(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        res = file.read()
    return res.splitlines()


def set_list_to_txt(filePath, urls):
    with open(filePath, 'a+', encoding='utf-8') as fileObject:
        for url in urls:
            fileObject.write(str(url))
            fileObject.write('\n')
        fileObject.close()
        drop_duplicates_txt(filePath)


def content_afterProcess_if_empty(origin_size, content, url):
    byteLength = len(content)
    kbLength = byteLength / 1000
    if (origin_size - kbLength > 5):
        logger.warning('origin_size=' + str(origin_size))
        logger.warning(url + '\n处理后的文件小于2kb(实际大小' + str(kbLength) + 'kb), 请重试\n')
        # raise异常, 换ip继续
        # change_ip("用 cmd 命令 换ip... 可尝试")
        # 尝试退出python解释器, 有效关闭多线程, 但是使用起来不方便
        # os._exit(-1)


def save_single_url(newUrl, referer):
    global COUNT_IF_DETACTED_BY_MANAGER
    try:
        # 随机休息0~2.0s
        randomSleep(2)
        # logger.debug(newUrl)
        # 获取网页
        html = getHtml(newUrl, referer)  # 调用函数获取bytes
        # logger.debug(html)

        # 替换为纯文本 并 删除多余内容 html2text
        # h = html2text.HTML2Text()
        # # Ignore converting links from HTML
        # h.ignore_links = True
        # h.ignore_images = True
        # h.s = True
        # h.d = True
        # content = h.handle(html)


        # 替换为纯文本  justext
        paragraphs = justext.justext(html, justext.get_stoplist("English"))
        for paragraph in paragraphs:
            if not paragraph.is_boilerplate:
                page_content = paragraph.text
                # print( content)

        # print()


        # 暂时放弃数据清洗, 等全部下载完成再进行数据清洗
        # byteLength = len(content)
        # origin_size = byteLength / 1000
        # if origin_size == 0:
        #     COUNT_IF_DETACTED_BY_MANAGER = COUNT_IF_DETACTED_BY_MANAGER + 1
        #     if COUNT_IF_DETACTED_BY_MANAGER == 3:
        #         logger.error('可能被识别为爬虫, 没有取到数据')
        #         os._exit(-1)
        # else:
        #     COUNT_IF_DETACTED_BY_MANAGER = 0
        #
        # # 跳过 recipes  部分
        # if "/recipes/" in newUrl:
        #     return
        #
        # # 删除掉 网页 上边栏 直至标题 之前
        # content = re.sub(r'^[\s\S]*?Log in[\s\S]*?(\*[ ]{3,})+[\s\S]{1,2000}\# ', "", content)
        # # 删除掉 网页 下边栏 广告
        # content = re.sub(r'Advertisement[^a-z]*Continue reading the main story[^a-z]*Site Index[\s\S]+', "", content)
        #
        # content_afterProcess_if_empty(origin_size,content, newUrl)

        # 正则 拆解 网页链接
        try:
            # 作废 --> 如果没有日期 就添加日期再 正则
            reg = r'([\d\/]{8,})\/([\w-]+)(\/[\w\d-]{3,})?\/([\w-]{3,}).*?searchResultPosition.(\d+)'
            newUrl_filter = re.findall(reg, newUrl)[0]
        except:
            # 保存 解析错误的 url
            tempList = []
            tempList.append(newUrl)
            set_list_to_txt('resource/forgive_url.txt', tempList)
            return
            # # 从content中 获取 日期
            # dateStr = re.findall(r'Published ([\w\W]+ [0-9]+..[0-9]+)\n', content)[0]
            # time_original = dateStr.replace('.', '').replace(',', '').replace(' ', '')
            # time_format = datetime.datetime.strptime(time_original, '%B%d%Y')
            # # 这里可以 print time_format 或者 直接 time_format 一下看看输出结果，默认存储为datetime格式
            # time_format = time_format.strftime('%Y/%m/%d/')
            # newUrl = newUrl.replace('com/', 'com/' + time_format)
            # reg = r'([\d\/]{8,})\/([\w-]+)(\/[\w\d-]{3,})?\/([\w-]{3,}).*?searchResultPosition.(\d+)'
            # newUrl_filter = re.findall(reg, newUrl)[0]

        # 把新文件名拼起来 id
        new_filename = newUrl_filter[0].replace('/', '') + '-' + newUrl_filter[1] + '-' + \
                       newUrl_filter[4].zfill(10)
        with open('resource/' + validateTitle('index') + '.txt', 'a+', encoding='utf-8') as file:
            title_for_index = newUrl_filter[1] + '/' + newUrl_filter[3]
            index_info = new_filename + "," + title_for_index
            file.write(index_info + "\n")
        with open('resource/down/' + validateTitle(new_filename) + '.txt', 'w', encoding='utf-8') as file:
            file.write('<url='+newUrl+'>\n'+str(page_content))
        logger.debug(newUrl + "已完成\n")
        return newUrl
    except SystemExit as e:
        logger.exception(e)
        logger.error("\n --------------此ip已被甄别, 达到爬取极限, 请标记后换一个--------------\n")
    except Exception as e:
        # 判断是不是常见错误 mid, 是则忽略
        res = re.search(r'.*mid[0-9]+$', newUrl)
        if res is None:
            # 忽略 politics 的视频
            logger.error(e)
            logger.debug(traceback.print_exc())
            logger.warning(newUrl + "本文章为空 \n")
            return ''
        else:
            return newUrl


def next_domain_url():
    raise GeneratorExit


def if_crawl_end_current_url(urls):
    global SAVE_LAST_URLS
    global COUNT_IF_CRAWL_END_CURRENT_URL
    if SAVE_LAST_URLS == urls:
        COUNT_IF_CRAWL_END_CURRENT_URL = COUNT_IF_CRAWL_END_CURRENT_URL + 1
    else:
        SAVE_LAST_URLS = urls
        COUNT_IF_CRAWL_END_CURRENT_URL = 0

def forgive_url_save2txt(url):
    tempList = []
    tempList.append(url)
    set_list_to_txt('resource/forgive_url.txt', tempList)

def nytime_process(driver, referer):
    # 打开网页 打
    time.sleep(3)
    driver.get(domain_url)

    urls_already = []
    urls_already.extend(get_list_from_txt('resource/alreadySave_url.txt'))

    while (1):
        # 连续三次 点击下一页未出现新东西. 则 用新的 domain_url 爬取数据
        # 情况1 点击下一页 刷不出新的
        if COUNT_IF_CRAWL_END_CURRENT_URL == 3:
            break
        # 获取 链接
        urls_obj = driver.find_elements_by_xpath('//ol[@data-testid="search-results"]/li//a')

        if_crawl_end_current_url(urls_obj)

        for url_obj in urls_obj:
            try:
                url = url_obj.get_attribute('href')
            except Exception as e:
                logger.error(e)
                continue
            # 跳过 常见 错误网址
            if 'www.nytimes.com' not in url:
                forgive_url_save2txt(url)
                continue
            # elif '/politics/' in url:
            # forgive_url_save2txt(url)
            #     continue
            # 跳过 视频
            elif '/video/' in url:
                forgive_url_save2txt(url)
                continue
            # 跳过 常见无效url
            elif 'after-dfp-ad-mid' in url:
                forgive_url_save2txt(url)
                continue
            # 跳过 url 里面没有日期的
            elif 'com/2' not in url:
                forgive_url_save2txt(url)
                continue

            if url not in urls_already:
                logger.debug(url)
                saved_url = save_single_url(url, referer)
                urls_already.append(saved_url)

        scroll2bottom(driver)
        set_list_to_txt('resource/alreadySave_url.txt', urls_already)

        # chrome 有效
        # js = "$('div.css-vsuiox>button').click()"
        # # 调用js 模拟点击下一页
        # driver.find_element_by_css_selector(js)

        # firefox 有效
        # 调用css定位 模拟点击下一页
        # 情况2 下一页 图标消失
        try:
            driver.find_element_by_css_selector('div.css-vsuiox>button').click()
        except Exception as e:
            logger.info(referer + " 此domain_url爬取完毕, next........")
            break


def wait_enterKey_to_continue():
    flag = True
    while (flag):
        inputKey = input("Press Enter (2次) to continue...\n")
        if (inputKey == ''):
            flag = False
            print('continue....\n')




def init():
    folder_path_list = [
        "utils",
        "resource",
        "resource/down/",
        "resource/old/",
    ]
    for folder_path in folder_path_list:
        if not os.path.exists(folder_path):  # 判断是否存在文件夹如果不存在则创建为文件夹
            os.makedirs(folder_path)
    templist = []
    set_list_to_txt("resource/alreadySave_domain_url.txt",templist)
    set_list_to_txt("resource/start_domain_url_list.txt",templist)
    set_list_to_txt("resource/alreadySave_url.txt",templist)
    set_list_to_txt("resource/forgive_url.txt",templist)


if __name__ == "__main__":
    init()
    COUNT_IF_DETACTED_BY_MANAGER = 0
    COUNT_IF_CRAWL_END_CURRENT_URL = 0
    SAVE_LAST_URLS = []

    domain_url_list = get_list_from_txt('resource/start_domain_url_list.txt')
    domain_url_list.reverse()

    driver = firefox_init()
    logger.info("启动完毕... 开始 跑程序...")
    # # 跳过 recipes  部分
    # logger.info('没有下载 recipes 部分 ')
    logger.info(' 等全部下载完成再进行数据清洗')
    logger.info(' 先登录 再进入')

    time.sleep(2)
    wait_enterKey_to_continue()
    driver.get('https://www.nytimes.com/search?dropmab=true&endDate=20200816&query=&sort=best&startDate=20070101')

    while (1):
        alreadySave_domain_url = get_list_from_txt('resource/alreadySave_domain_url.txt')
        domain_url = domain_url_list.pop()
        if domain_url in alreadySave_domain_url:
            continue
        try:
            nytime_process(driver, domain_url)
            templist = []
            templist.append(domain_url)
            set_list_to_txt('resource/alreadySave_domain_url.txt', templist)
        except Exception as e:
            logger.exception(e)
    # driver.quit()
