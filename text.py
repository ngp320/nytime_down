# import random
# import re
# import sys
#
# import requests
# from loguru import logger
# from lxml import etree
#
#
# def getHtml(url, referer):  # 获取html的内容
#     from utils.userAgents import MY_USER_AGENT
#     useragent = random.choice(MY_USER_AGENT)
#     proxies = {"http": "http://127.0.0.1:7890", "https": "http://127.0.0.1:7890"}
#     headers = {
#         'User-Agent': useragent,  ##从self.user_agent_list中随机取出一个字符串
#         'Referer': referer,
#         'Connection': 'close'  # 关闭长连接
#     }  ##构造成一个完整的User-Agent （UA代表的是上面随机取出来的字符串哦）
#     html = requests.get(url, headers=headers, proxies=proxies, verify=False)
#     return html.text  # http.client.HTTPResponse的变量
#
#
# def setLoguru():
#     fmt = "<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green> " \
#           "<level><red>|</red>" \
#           "{level}<red>|</red>" \
#           "{file}<red>:</red> " \
#           "{function} <red>:</red>" \
#           "L{line} " \
#           "{message}</level> "
#
#     #  多线程 就 显示线程吧, 现在已经有点拥挤了
#     # fmt = "<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green> " \
#     #       "<level><red>|</red> " \
#     #       "{level} <red>|</red> " \
#     #       "{thread.name}<red>:</red>" \
#     #       "{thread.id} <red>|</red> " \
#     #       "{file}<red>:</red> " \
#     #       "{function} <red>:</red>" \
#     #       "L{line} - " \
#     #       "{message}</level> "
#     # logger.add 不能分多行
#     logger.remove(handler_id=None)  # 清除之前的设置
#     # logger.add(sys.stderr, format=fmt, level="INFO", colorize=True)
#     logger.add(sys.stderr, format=fmt, level="DEBUG", colorize=True)
#     # 设置生成日志文件，每天10MB切割，保留3天，utf-8编码，异步写入，zip压缩
#     logger.add("logs/runtime.log", level="DEBUG", backtrace=True, rotation='10MB', retention='3 days',
#                encoding="utf-8", enqueue=True, compression="zip")
#     # log日志 分几层, 后期容易处理
#     logger.add("logs/runError.log", level="ERROR", encoding="utf-8")
#     logger.add("logs/runWarning.log", level="WARNING", encoding="utf-8")
#     logger.add("logs/runInfo.log", level="INFO", encoding="utf-8")
#
# def simple_content_tool(content):
#     from bs4 import BeautifulSoup
#     '''
#     得到文章的简略信息
#     :param content:
#     :return:
#     '''
#     simple_info = BeautifulSoup(content, 'html.parser')
#     return simple_info.get_text()
#
# import html2text
#
# if __name__ == '__main__':
#     setLoguru()
#     referer = 'https://www.nytimes.com/search?dropmab=true&endDate=20201231&query=&sort=best&startDate=20200101'
#     url = '   https://www.nytimes.com/article/most-popular-recipes-2020.html?searchResultPosition=23       '
#     html = getHtml(url, referer)
#     # logger.info(html)
#
#     # logger.info(html2text.html2text(html))
#     h = html2text.HTML2Text()
#     # Ignore converting links from HTML
#     h.ignore_links = True
#     h.ignore_images = True
#     h.s = True
#     h.d = True
#     content = h.handle(html)
#     # dateStr = re.findall(r'Published ([\w\W]+ [0-9]+..[0-9]+)\n',content)
#     # print(dateStr)
#     print(content)
#
#
#     # 删除掉 网页 上边栏 直至标题 之前
#     # content = re.sub(r'^[\s\S]*?Log in[\s\S]*?(\*[ ]{3,})+[\s\S]{1,2000}\# ', "", content)
#     # # # 删除掉 网页 下边栏 广告
#     # content = re.sub(r'Advertisement[^a-z]*Continue reading the main story[^a-z]*Site Index[\s\S]+', "", content)
#     # logger.info(content)
#
# # https://www.nytimes.com/interactive/2020/12/29/climate/new-years-resolutions-climate.html?searchResultPosition=275 出问题了... ?
#
# # import re
# # newUrl = 'https://www.nytimes.com/2020/12/30/wirecutter/guides/how-to-clean-a-laptop/?searchResultPosition=152  '
# # # newUrl = 'https://www.nytimes.com/wirecutter/guides/how-to-clean-a-laptop/?searchResultPosition=152 '
# # # newUrl = 'https://www.nytimes.com/interactive/2020/12/02/arts/design/new-york-city-walking-tours.html?searchResultPosition=579'
# # reg = r'([\d\/]{8,})\/([\w-]+)(\/[\w-]{3,})?\/([\w-]{3,}).*?searchResultPosition.(\d+)'
# # newUrl_filter = re.findall(reg, newUrl)[0]
# # # 把新文件名拼起来 id
# # new_filename = newUrl_filter[0].replace('/','')  + '-' + newUrl_filter[1] + '-' + \
# #                newUrl_filter[4].zfill(10)
# # title_for_index = newUrl_filter[1] + '/' + newUrl_filter[3]
# # index_info = new_filename + "," + title_for_index
# #
# # print(title_for_index)
# # print(index_info)
# #
# # 日期处理
# # import time,datetime
# #
# # time_original = 'Dec. 30, 2020 '.replace('.','').replace(',','').replace(' ','')
# # time_format = datetime.datetime.strptime(time_original, '%b%d%Y')
# # #这里可以 print time_format 或者 直接 time_format 一下看看输出结果，默认存储为datetime格式
# # time_format = time_format.strftime('%Y/%m/%d')
# # print( time_format)

def drop_duplicates_txt(filePath):
    tmp = []
    with open(filePath, encoding="utf-8") as file:
        for line in file.readlines():
            line = line.strip('\n')
            tmp.append(line)
    tmp = list(set(tmp))
    tmp = sorted(tmp)
    tmp.reverse()
    with open(filePath, 'w', encoding='utf-8') as fileObject:
        for url in tmp:
            fileObject.write(str(url))
            fileObject.write('\n')
        fileObject.close()

drop_duplicates_txt('resource/start_domain_url_list.txt')