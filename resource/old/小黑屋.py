################# 浏览器 窗口切换 ##############
# 浏览器 新窗口打开连接
# newwindow = 'window.open("' + newUrl + '")'
# time.sleep(1)
# driver.execute_script(newwindow)
# # 移动句柄，对新打开页面进行操作
# driver.switch_to_window(driver.window_handles[1])
# # 具体操作 下载页面
# ..............
# # 关闭该新打开的页面
# driver.close()
# # # 不关闭，要移动到上一个页面，我们要移动句柄
# # driver.switch_to_window(driver.window_handles[0])

##########  html通过lxml可使用css选择器提取文本#########
# logger.debug(html)
# obj = etree.HTML(str(html))
# # //*[@id="story"]/header/div[3]//text()
# # //*[@id="story"]/section//text()
# title = obj.xpath('//*[@id="story"]/header/div[3]//text()')
# title = "\n".join(title)
# main_body = obj.xpath('//*[@id="story"]/section//text()')
# # main_body_if_empty(main_body, newUrl)
# main_body = "\n".join(main_body)
# logger.info(title)
# # logger.info(main_body)

# 日期生成
# import pandas as pd
# from datetime import datetime
#
#
# def datelist(beginDate, endDate):
#     # beginDate, endDate是形如‘20160601’的字符串或datetime格式
#     date_l = [datetime.strftime(x, '%Y-%m-%d') for x in list(pd.date_range(start=beginDate, end=endDate))]
#     return date_l
#
#
# if __name__ == '__main__':
#     date_l = datelist('20200101', '20201231')
#     target_list = []
#     for date in date_l:
#         target_list.append('https://www.nytimes.com/'+date.replace('-','/'))
#     target_list.reverse()
#     print(target_list)



# 删除 未成功 下载的 url
# list_wrong =  []
#
# list_already = []
# tar = []
# for url in list_already:
#     if url not in list_wrong:
#         tar.append(url)
# with open('1.txt','w') as f:
#     f.write("\n".join(tar))
#


# 临时处理 url的拆解
# import re
# newUrl = 'https://www.nytimes.com/wirecutter/guides/how-to-clean-a-laptop/?searchResultPosition=152 '
# # newUrl = 'https://www.nytimes.com/interactive/2020/12/02/arts/design/new-york-city-walking-tours.html?searchResultPosition=579'
# reg = r'([\d\/]{8,})\/([\w-]+)(\/[\w-]{3,})?\/([\w-]{3,}).html.searchResultPosition.(\d+)'
# newUrl_filter = re.findall(reg, newUrl)[0]
# # 把新文件名拼起来 id
# new_filename = newUrl_filter[0].replace('/','')  + '-' + newUrl_filter[1] + '-' + \
#                newUrl_filter[4].zfill(10)
# title_for_index = newUrl_filter[1] + '/' + newUrl_filter[3]
# index_info = new_filename + "," + title_for_index
#
# print(title_for_index)
# print(index_info)
