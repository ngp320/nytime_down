from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import time
from bs4 import BeautifulSoup
import re
import math
import os

s = Service('D:/word2vec/drivers/chromedriver.exe')
option = webdriver.ChromeOptions()
option.add_argument("--user-data-dir=" + r"D:/word2vec/User Data/")
option.add_argument('--no-sandbox')
driver = webdriver.Chrome(service=s, options=option)


loanword="mao tai"
URLs=list()
sort="newest"
driver.get('https://www.nytimes.com/search?dropmab=false&query="'+loanword+'"&sort='+sort)
time.sleep(2)
loanWords_page = BeautifulSoup(driver.page_source, 'lxml')
pageNum=loanWords_page.find("p",{"data-testid":"SearchForm-status"})
str = pageNum.text
match = re.search('Showing ([0-9,]+) results for', str)
totalLoanWords = match.group(1) if match else None
totalLoanWords = re.sub(",", "", totalLoanWords)
print(totalLoanWords)
clickNum=math.floor(int(totalLoanWords)/10)

for x in range(clickNum):
    driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
    time.sleep(4)
    driver.find_element(By.CSS_SELECTOR, 'div.css-vsuiox>button').click()
    time.sleep(5)
    bsobj = BeautifulSoup(driver.page_source, 'lxml')
    for link in bsobj.find_all('a'):
        match = re.search("searchResultPosition=", link.get('href'))
        if match:
            thisLink = re.sub("\?searchResultPosition=[0-9]+", "", link.get('href'))
            if (thisLink not in URLs):
                URLs.append(thisLink)
    time.sleep(2)

sort="oldest"
driver.get('https://www.nytimes.com/search?dropmab=false&query="'+loanword+'"&sort='+sort)
for x in range(clickNum):
    driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
    time.sleep(4)
    driver.find_element(By.CSS_SELECTOR, 'div.css-vsuiox>button').click()
    time.sleep(5)
    bsobj = BeautifulSoup(driver.page_source, 'lxml')
    for link in bsobj.find_all('a'):
        match = re.search("searchResultPosition=", link.get('href'))
        if match:
            thisLink = re.sub("\?searchResultPosition=[0-9]+", "", link.get('href'))
            if (thisLink not in URLs):
                URLs.append(thisLink)
    time.sleep(2)

with open('urls+'+totalLoanWords+'+'+loanword+'+unified+Chrome.txt', 'w') as f:
    f.write('\n'.join(URLs))
    # f.flush
    # os.fsync(f)

f.close()
driver.close()


